/********************************************************************************
 * Copyright (c) 2015-2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.api.odsadapter;

import static org.assertj.core.api.Assertions.assertThat;
import static org.eclipse.mdm.api.odsadapter.ODSContextFactory.PARAM_NAMESERVICE;
import static org.eclipse.mdm.api.odsadapter.ODSContextFactory.PARAM_PASSWORD;
import static org.eclipse.mdm.api.odsadapter.ODSContextFactory.PARAM_SERVICENAME;
import static org.eclipse.mdm.api.odsadapter.ODSContextFactory.PARAM_USER;

import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.mdm.api.base.ConnectionException;
import org.eclipse.mdm.api.base.ServiceNotProvidedException;
import org.eclipse.mdm.api.base.Transaction;
import org.eclipse.mdm.api.base.model.User;
import org.eclipse.mdm.api.dflt.ApplicationContext;
import org.eclipse.mdm.api.dflt.EntityManager;
import org.eclipse.mdm.api.dflt.model.EntityFactory;
import org.eclipse.mdm.api.dflt.model.Role;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Ignore
// FIXME 10.7.2017: this test needs a running ODS Server, that is not suitable for continous build in Jenkins.
// Comment this in for local tests only.
public class ODSRoleTest {

	/*
	 * ATTENTION: ==========
	 *
	 * To run this test make sure the target service is running a MDM default model
	 * and any database constraint which enforces a relation of Test to a parent
	 * entity is deactivated!
	 */

	private static final Logger LOGGER = LoggerFactory.getLogger(ODSRoleTest.class);

	private static final String NAME_SERVICE = "corbaloc::1.2@%s:%s/NameService";

	private static final String USER = "sa";
	private static final String PASSWORD = "sa";

	private static ApplicationContext context;
	private static EntityManager em;
	private static EntityFactory entityFactory;

	@BeforeClass
	public static void setUpBeforeClass() throws ConnectionException {
		String nameServiceHost = System.getProperty("host");
		String nameServicePort = System.getProperty("port");
		String serviceName = System.getProperty("service");

		if (nameServiceHost == null || nameServiceHost.isEmpty()) {
			throw new IllegalArgumentException("name service host is unknown: define system property 'host'");
		}

		nameServicePort = nameServicePort == null || nameServicePort.isEmpty() ? String.valueOf(2809) : nameServicePort;
		if (nameServicePort == null || nameServicePort.isEmpty()) {
			throw new IllegalArgumentException("name service port is unknown: define system property 'port'");
		}

		if (serviceName == null || serviceName.isEmpty()) {
			throw new IllegalArgumentException("service name is unknown: define system property 'service'");
		}

		Map<String, String> connectionParameters = new HashMap<>();
		connectionParameters.put(PARAM_NAMESERVICE, String.format(NAME_SERVICE, nameServiceHost, nameServicePort));
		connectionParameters.put(PARAM_SERVICENAME, serviceName + ".ASAM-ODS");
		connectionParameters.put(PARAM_USER, USER);
		connectionParameters.put(PARAM_PASSWORD, PASSWORD);

		context = new ODSContextFactory().connect(connectionParameters);
		em = context.getEntityManager().orElseThrow(() -> new ServiceNotProvidedException(EntityManager.class));
		entityFactory = context.getEntityFactory()
				.orElseThrow(() -> new IllegalStateException("Entity manager factory not available."));
	}

	@AfterClass
	public static void tearDownAfterClass() throws ConnectionException {
		if (context != null) {
			context.close();
		}
	}

	@org.junit.Test
	public void testLoadRelatedUsers()
			throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		User user = em.loadAll(User.class, "sa").get(0);
		List<Role> role = em.loadRelatedEntities(user, "users2groups", Role.class);

		assertThat(role).hasSize(1).extracting(Role::getName).containsExactly("MDMSystemAdministrator");
	}

	@org.junit.Test
	public void testLoadRelatedRoles()
			throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		Role role = em.loadAll(Role.class, "MDMSystemAdministrator").get(0);
		List<User> user = em.loadRelatedEntities(role, "groups2users", User.class);

		assertThat(user).hasSize(1).extracting(User::getName).containsExactly("sa");
	}

	@org.junit.Test
	public void testCreateUserAndRole()
			throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		String userName = "CreateUserAndRole";
		String roleName = "CreateUserAndRole";

		try {
			Transaction transaction = em.startTransaction();
			User user = entityFactory.createUser(userName, "Test", "User");
			Role role = entityFactory.createRole(roleName);
			role.addUser(user);

			transaction.create(Arrays.asList(user, role));
			transaction.commit();

			assertThat(em.loadRelatedEntities(role, "users2groups", User.class)).hasSize(1).extracting(User::getName)
					.contains(userName);

			assertThat(em.loadRelatedEntities(user, "groups2users", Role.class)).hasSize(1).extracting(Role::getName)
					.contains(roleName);
		} finally {
			Transaction transaction = em.startTransaction();
			List<User> users = em.loadAll(User.class, userName);
			transaction.delete(users);
			List<Role> roles = em.loadAll(Role.class, roleName);
			transaction.delete(roles);
			transaction.commit();
		}
	}

	@org.junit.Test
	public void testAddMultipleUsersToNewRole()
			throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		String roleName = "AddMultipleUsersToNewRole";
		try {
			Transaction transaction = em.startTransaction();
			List<User> users = em.loadAll(User.class);
			Role role = entityFactory.createRole(roleName);

			users.forEach(u -> role.addUser(u));
			transaction.create(Arrays.asList(role));
			transaction.commit();

			assertThat(em.loadRelatedEntities(role, "users2groups", User.class)).hasSize(users.size());
		} finally {
			Transaction transaction = em.startTransaction();
			List<Role> roles = em.loadAll(Role.class, roleName);
			transaction.delete(roles);
			transaction.commit();
		}
	}

	@org.junit.Test
	public void testRemoveUserFromRole()
			throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		String userName = "RemoveUserFromRole";
		try {
			Transaction transaction = em.startTransaction();
			Role role = em.load(Role.class, "1");
			User user = entityFactory.createUser(userName, "User", "User");

			role.addUser(user);

			transaction.create(Arrays.asList(user));
			transaction.update(Arrays.asList(role));
			transaction.commit();

			assertThat(em.loadRelatedEntities(role, "groups2users", User.class)).hasSize(2);

			Role role2 = em.load(Role.class, "1");
			transaction = em.startTransaction();
			role2.removeUser(user);
			transaction.update(Arrays.asList(role2));
			transaction.commit();

			assertThat(em.loadRelatedEntities(role2, "groups2users", User.class)).hasSize(1);
		} finally {
			Transaction transaction = em.startTransaction();
			List<User> users = em.loadAll(User.class, userName);
			transaction.delete(users);
			transaction.commit();
		}
	}
}
